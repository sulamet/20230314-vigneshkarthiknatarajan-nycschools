//
//  NYCSchoolServiceHandlerObjc.m
//  NYCSchoolInfo
//
//  Created by Vigneshkarthik Natarajan on 3/15/23.
//

#import "NYCSchoolServiceHandlerObjc.h"

@implementation NYCSchoolServiceHandlerObjc

+ (void)fetchResponseDataFor:(NSURL *)url withCompletion:(void(^)(NSData * _Nullable data, NSError * _Nullable error))callback {
        
    NSURLSessionDataTask *downloadTask = [[NSURLSession sharedSession]
      dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (data != nil) {
            callback(data, nil);
        } else {
            callback(nil, error);
        }
    }];
        
    [downloadTask resume];
}

@end
