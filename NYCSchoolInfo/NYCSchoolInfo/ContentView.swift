//
//  ContentView.swift
//  NYCSchoolInfo
//
//  Created by Vigneshkarthik Natarajan on 3/14/23.
//

import SwiftUI

struct ContentView: View {
    @State var isActive = true
    var body: some View {
        
        /// Displays splash view initially and moves to Landing view after delay
        ZStack {
            if isActive {
                SplashView()
            } else {
                NYCSchoolListLandingView()
            }
        }
        .onAppear(perform: {
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
                isActive = false
            }
        })
    }
}
