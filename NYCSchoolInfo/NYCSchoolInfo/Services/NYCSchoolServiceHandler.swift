//
//  NYCSchoolServiceHandler.swift
//  NYCSchoolInfo
//
//  Created by Vigneshkarthik Natarajan on 3/14/23.
//

import Foundation

enum ServiceError: Error {
    case noData
}

class NYCSchoolServiceHandler {
    
    /// Method used to fetch and map response from service api
    /// - Parameters:
    ///   - url: URL
    ///   - responseType: Response class type
    ///   - completion: completion callback returns response and Error
    func fetchData(url: URL,
                   completion: @escaping(Result<Data?, Error>) -> Void) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            if let data = data {
                completion(.success(data))
            } else {
                completion(.failure(ServiceError.noData))
            }
        }.resume()
    }
}

class NYCSchoolResponseHandler {
    
    /// Method used to fetch and map response from service api
    /// - Parameters:
    ///   - url: URL
    ///   - responseType: Response class type
    ///   - completion: completion callback returns response and Error
    func mapData<T: Codable>(data: Data?,
                             responseType : T.Type,
                             completion: @escaping(Result<T, Error>) -> Void) {
        if let data = data {
            do {
                let response = try JSONDecoder().decode(T.self, from: data)
                completion(.success(response))
            } catch let error {
                completion(.failure(error))
            }
        }
    }
}
