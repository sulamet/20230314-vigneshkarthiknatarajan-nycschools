//
//  NYCSchoolInfoTests.swift
//  NYCSchoolInfoTests
//
//  Created by Vigneshkarthik Natarajan on 3/14/23.
//

import XCTest

final class NYCSchoolInfoTests: XCTestCase {
    var schoolListModel: [NYCSchoolListModel]!
    var schoolDetailModel: [NYCSchoolDetailsModel]!
    var schoolListViewModel: NYCSchoolListViewModel!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        self.schoolListModel = try TestDataLoader.loadObjectFromFile(objectType: [NYCSchoolListModel].self,
                                                                     fileName: "NYCSchoolList")
        self.schoolDetailModel = try TestDataLoader.loadObjectFromFile(objectType: [NYCSchoolDetailsModel].self,
                                                                       fileName: "NYCSchoolSATScore")
        self.schoolListViewModel = NYCSchoolListViewModel()
        self.schoolListViewModel.handleSchoolListResponse(response: schoolListModel)
        self.schoolListViewModel.handleSchoolDetailsResponse(response: schoolDetailModel)
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        self.schoolListModel = nil
        self.schoolDetailModel = nil
        self.schoolListViewModel = nil
    }
    
    func testGetSchoolSATScoreWhenSATScoreIsAvailable() {
        let schoolDBN = "21K728"
        let satScoreArr = self.schoolListViewModel.getSchoolSATScore(dbn: schoolDBN)
        XCTAssertNotEqual(satScoreArr?.count, 0)
    }
    
    func testGetSchoolSATScoreWhenSATScoreIsNotAvailable() {
        let schoolDBN = "02M260"
        let satScoreArr = self.schoolListViewModel.getSchoolSATScore(dbn: schoolDBN)
        XCTAssertEqual(satScoreArr?.count, 0)
    }
}
