//
//  NYCSchoolDetailView.swift
//  NYCSchoolInfo
//
//  Created by Vigneshkarthik Natarajan on 3/14/23.
//

import SwiftUI

/// NYCSchoolDetailView - Displays school detail view
struct NYCSchoolDetailView: View {
    var school: SchoolDetail
    var schoolSATScore: SchoolSATScoreDetail
    @State private var isExpanded = false

    var body: some View {
        ScrollView(content: {
            VStack(alignment: .leading, spacing: 0, content:  {
                SchoolInfoView(school: school)
                    .padding(EdgeInsets(top: NYCSchoolConstants.NumericalValues.maxPadding(),
                                        leading: NYCSchoolConstants.NumericalValues.maxPadding(),
                                        bottom: .zero,
                                        trailing: NYCSchoolConstants.NumericalValues.maxPadding()))
                    .cornerRadius(NYCSchoolConstants.NumericalValues.cornerRadius())
                SchoolContactInfoView(school: school)
                    .padding(EdgeInsets(top: NYCSchoolConstants.NumericalValues.maxPadding(),
                                        leading: NYCSchoolConstants.NumericalValues.maxPadding(),
                                        bottom: .zero,
                                        trailing: NYCSchoolConstants.NumericalValues.maxPadding()))
                    .cornerRadius(NYCSchoolConstants.NumericalValues.cornerRadius())
                SchoolSATInfoView(school: school,
                                  schoolSATScore: schoolSATScore)
                .padding(EdgeInsets(top: NYCSchoolConstants.NumericalValues.maxPadding(),
                                    leading: NYCSchoolConstants.NumericalValues.maxPadding(),
                                    bottom: .zero,
                                    trailing: NYCSchoolConstants.NumericalValues.maxPadding()))
                .cornerRadius(NYCSchoolConstants.NumericalValues.cornerRadius())
            })
            .navigationTitle(school.name)
        })
        .background(Color("default_background"))
    }
}

/// SchoolInfoView - Displays school over view
struct SchoolInfoView: View {
    var school: SchoolDetail
    var body: some View {
        VStack(alignment: .leading, spacing: 0, content: {
            Text("school_overview")
                .fontWeight(.bold)
                .padding(.top, NYCSchoolConstants.NumericalValues.minPadding())
                .padding(.leading, NYCSchoolConstants.NumericalValues.maxPadding())
            Text(school.overviewParagraph)
                .multilineTextAlignment(.leading)
                .lineLimit(nil)
                .padding(.vertical, NYCSchoolConstants.NumericalValues.maxPadding())
                .padding(.horizontal, NYCSchoolConstants.NumericalValues.maxPadding())
        })
        .background(Color("default_white"))
    }
}

/// SchoolContactInfoView - Displays schools contact information
struct SchoolContactInfoView: View {
    var school: SchoolDetail
    var body: some View {
        VStack(alignment: .leading, spacing: 0, content: {
            Text("school_contact_info")
                .fontWeight(.bold)
                .padding(.vertical, NYCSchoolConstants.NumericalValues.maxPadding())
                .padding(.leading, NYCSchoolConstants.NumericalValues.maxPadding())
            SchoolDetailInfoCellView(label: "school_contact_info_phone", value: school.phoneNumber)
            SchoolDetailInfoCellView(label: "school_contact_info_fax", value: school.faxNumber)
            SchoolDetailInfoCellView(label: "school_contact_info_email", value: school.schoolEmail)
            SchoolDetailInfoCellView(label: "school_contact_info_web", value: school.website)
            SchoolDetailInfoCellView(label: "school_contact_info_address", value: school.primaryAddress)
        })
        .background(Color("default_white"))
    }
}

/// SchoolSATInfoView - Displays school sat score details
struct SchoolSATInfoView: View {
    var school: SchoolDetail
    var schoolSATScore: SchoolSATScoreDetail
    var body: some View {
        VStack(alignment: .leading, spacing: 0, content: {
            Text("sat_section_title")
                .fontWeight(.bold)
                .padding(.vertical, NYCSchoolConstants.NumericalValues.maxPadding())
                .padding(.leading, NYCSchoolConstants.NumericalValues.maxPadding())
            SchoolDetailInfoCellView(label: "sat_section_writing_score", value: schoolSATScore.writingAvgScore)
            SchoolDetailInfoCellView(label: "sat_section_math_score", value: schoolSATScore.mathAvgScore)
            SchoolDetailInfoCellView(label: "sat_section_reading_score", value: schoolSATScore.readingAvgScore)
        })
        .background(Color("default_white"))
    }
}

/// SchoolDetailInfoCellView - Displays school details cell view
struct SchoolDetailInfoCellView: View {
    var label: String
    var value: String

    var body: some View {
        VStack(alignment: .leading, spacing: 0, content:  {
            HStack(content: {
                Text(label.localized)
                    .fontWeight(.bold)
                Spacer()
                Text(value.localized)
                    .padding(.trailing, NYCSchoolConstants.NumericalValues.maxPadding())
                    .onAppear(perform: {
                        print(value)
                    })
            })
            .padding(.vertical, NYCSchoolConstants.NumericalValues.minPadding())
            .padding(.horizontal, NYCSchoolConstants.NumericalValues.maxPadding())
            Divider()
                .frame(height: NYCSchoolConstants.NumericalValues.dividerHeight())
                .padding(.leading, NYCSchoolConstants.NumericalValues.maxPadding())
        })
    }
}

