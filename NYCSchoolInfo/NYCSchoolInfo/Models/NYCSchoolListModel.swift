//
//  NYCSchoolListModel.swift
//  NYCSchoolInfo
//
//  Created by Vigneshkarthik Natarajan on 3/14/23.
//

import Foundation

struct NYCSchoolListModel: Codable {
    private enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case overviewParagraph = "overview_paragraph"
        case academicopportunitiesOne = "academicopportunities1"
        case academicopportunitiesTwo = "academicopportunities2"
        case phoneNumber = "phone_number"
        case faxNumber = "fax_number"
        case schoolEmail = "school_email"
        case website = "website"
        case finalgrades
        case totalStudents = "total_students"
        case primaryAddressLine = "primary_address_line_1"
        case city = "city"
        case zip = "zip"
        case stateCode = "state_code"
        case borough = "borough"
    }
    
    let dbn: String
    let schoolName: String
    let overviewParagraph: String
    let academicopportunitiesOne: String?
    let academicopportunitiesTwo: String?
    let phoneNumber: String
    let faxNumber: String?
    let schoolEmail: String?
    let website: String
    let finalgrades: String
    let totalStudents: String
    let primaryAddressLine: String
    let city: String
    let zip: String
    let stateCode: String
    let borough: String?
}
