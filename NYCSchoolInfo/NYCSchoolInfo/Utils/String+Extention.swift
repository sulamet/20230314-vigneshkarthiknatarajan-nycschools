//
//  String+Extention.swift
//  NYCSchoolInfo
//
//  Created by Vigneshkarthik Natarajan on 3/14/23.
//

import Foundation

extension String {
    
    /// Gives localized string value
    var localized: String {
        NSLocalizedString(self, comment: "")
    }
}
